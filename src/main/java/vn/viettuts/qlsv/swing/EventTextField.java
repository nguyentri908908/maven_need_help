package vn.viettuts.qlsv.swing;

public interface EventTextField {

    public void onPressed(EventCallBack call);

    public void onCancel();
}
