package vn.viettuts.qlsv.swing.raven.datechooser;

public interface EventDateChooser {

    public void dateSelected(SelectedAction action, SelectedDate date);
}
