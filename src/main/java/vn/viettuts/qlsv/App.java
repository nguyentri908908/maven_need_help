package vn.viettuts.qlsv;

import vn.viettuts.qlsv.view.DashBoardView;
import vn.viettuts.qlsv.view.LoginView;
import vn.viettuts.qlsv.controller.DashBoardController;
import vn.viettuts.qlsv.controller.LoginController;
import java.io.*;
import java.awt.EventQueue;
public class App {
    public static void main(String[] args) {

        EventQueue.invokeLater(new Runnable() {
            public void run() {

                LoginView view = new LoginView();
                LoginController controller = new LoginController(view);
                // hiển thị màn hình login
                controller.showLoginView();
//                DashBoardView view = new DashBoardView();
//                DashBoardController controller = new DashBoardController(view);
//                controller.showDashView();
            }
        });
    }
}