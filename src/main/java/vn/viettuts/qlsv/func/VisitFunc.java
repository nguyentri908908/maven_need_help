package vn.viettuts.qlsv.func;
import vn.viettuts.qlsv.entity.Crime;
import vn.viettuts.qlsv.entity.RegisterToVisit;
import vn.viettuts.qlsv.entity.Prison;
import vn.viettuts.qlsv.entity.RegisterToVisitXML;
import vn.viettuts.qlsv.utils.FileUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class VisitFunc {
    private static final String RegisterToVisit_FILE_NAME = "RegisterToVisit.xml";
    private List<RegisterToVisit> listRegisterToVisits = this.readListRegisterToVisits();

    public VisitFunc() {
    }

    public void writeListRegisterToVisits(List<RegisterToVisit> RegisterToVisits) {
        System.out.println("write");
        RegisterToVisitXML xml = new RegisterToVisitXML();
        xml.setRegisterToVisit(RegisterToVisits);
        FileUtils.writeXMLtoFile("RegisterToVisit.xml", xml);
    }

    public List<RegisterToVisit> readListRegisterToVisits() {
        List<RegisterToVisit> list = new ArrayList();
        RegisterToVisitXML xml = (RegisterToVisitXML) FileUtils.readXMLFile("RegisterToVisit.xml", RegisterToVisitXML.class);
        if (xml != null) {
            list = xml.getRegisterToVisit();
        }

        return (List)list;
    }
    public int getLastId(){
        int max = -1;
        for(RegisterToVisit i: listRegisterToVisits){
            if(i.getId()>max) max=i.getId();
        }
        return max+1;
    }

    public void add(RegisterToVisit RegisterToVisit) {

        this.listRegisterToVisits.add(RegisterToVisit);
        this.writeListRegisterToVisits(this.listRegisterToVisits);
    }

    public void edit(RegisterToVisit RegisterToVisit) {
        int size = this.listRegisterToVisits.size();
        System.out.println(RegisterToVisit.getId());
        for(int i = 0; i < size; ++i) {
            if (((RegisterToVisit)this.listRegisterToVisits.get(i)).getId() == RegisterToVisit.getId()) {

                ((RegisterToVisit)this.listRegisterToVisits.get(i)).setNameOfVisiter(RegisterToVisit.getNameOfVisiter());
                ((RegisterToVisit)this.listRegisterToVisits.get(i)).setVisitLocation(RegisterToVisit.getVisitLocation());
                ((RegisterToVisit)this.listRegisterToVisits.get(i)).setPrisoner(RegisterToVisit.getPrisoner());
                ((RegisterToVisit)this.listRegisterToVisits.get(i)).setvisitDay(RegisterToVisit.getvisitDay());
                ((RegisterToVisit)this.listRegisterToVisits.get(i)).settimeStart(RegisterToVisit.gettimeStart());
                ((RegisterToVisit)this.listRegisterToVisits.get(i)).setHour(RegisterToVisit.getHour());
                ((RegisterToVisit)this.listRegisterToVisits.get(i)).setRelasionship(RegisterToVisit.getRelasionship());
                System.out.println(RegisterToVisit.getRelasionship());
                this.writeListRegisterToVisits(this.listRegisterToVisits);
                break;
            }
        }

    }


    public boolean delete(RegisterToVisit RegisterToVisit) {
        boolean isFound = false;
        int size = this.listRegisterToVisits.size();

        for(int i = 0; i < size; ++i) {
            if (((RegisterToVisit)this.listRegisterToVisits.get(i)).getId() == RegisterToVisit.getId()) {
                RegisterToVisit = (RegisterToVisit)this.listRegisterToVisits.get(i);
                isFound = true;
                break;
            }
        }

        if (isFound) {
            this.listRegisterToVisits.remove(RegisterToVisit);
            this.writeListRegisterToVisits(this.listRegisterToVisits);
            return true;
        } else {
            return false;
        }
    }


    public List<RegisterToVisit> getListVisitById(int Id){
        List<RegisterToVisit> result = new ArrayList<>();
        for (RegisterToVisit i: this.listRegisterToVisits) {
            if(i.getId()==Id){
                result.add(i);
            }
        }
        return result;
    }
    public List<RegisterToVisit> getListVisitByIdPrison(String Id){
        List<RegisterToVisit> result = new ArrayList<>();
        for (RegisterToVisit i: this.listRegisterToVisits) {
            if(i.getPrisoner().contains(Id)){
                result.add(i);
            }
        }
        return result;
    }
    public List<RegisterToVisit> getListVisitByVisitDay(String day){
        List<RegisterToVisit> result = new ArrayList<>();
        for (RegisterToVisit i: this.listRegisterToVisits) {
            if(i.getvisitDay().contains(day)){
                result.add(i);
            }
        }
        return result;
    }
    public List<RegisterToVisit> getListVisitByLocation(String loca){
        List<RegisterToVisit> result = new ArrayList<>();
        for (RegisterToVisit i: this.listRegisterToVisits) {
            if(i.getVisitLocation().contains(loca)){
                result.add(i);
            }
        }
        return result;
    }
    public List<RegisterToVisit> getListVisitByNameVisiter(String name){
        List<RegisterToVisit> result = new ArrayList<>();
        for (RegisterToVisit i: this.listRegisterToVisits) {
            if(i.getNameOfVisiter().contains(name)){
                result.add(i);
            }
        }
        return result;
    }
    public List<RegisterToVisit> getListVisitByRelasionship(String re){
        List<RegisterToVisit> result = new ArrayList<>();
        for (RegisterToVisit i: this.listRegisterToVisits) {
            if(i.getRelasionship().contains(re)){
                result.add(i);
            }
        }
        return result;
    }
    public List<RegisterToVisit> getListRegisterToVisits() {
        return this.listRegisterToVisits;
    }
    public List<RegisterToVisit> getListFilterYear(String year){
        List<RegisterToVisit> result = new ArrayList<>();
        for(RegisterToVisit i : this.listRegisterToVisits){
            String[] split = i.getvisitDay().split("-");
            if(split[2].equals(year)) result.add(i);
        }
        return result;
    }

    public List<RegisterToVisit> getListFilterMonth(String year,String month){
        List<RegisterToVisit> result = new ArrayList<>();
        for(RegisterToVisit i : this.listRegisterToVisits){
            String[] split = i.getvisitDay().split("-");
            if(split[2].equals(year)&& split[1].trim().equals(month)) result.add(i);
        }
        return result;
    }
    public List<RegisterToVisit> getListFilterDate(String year, String month, String date){
        List<RegisterToVisit> result = new ArrayList<>();
        for(RegisterToVisit i : this.listRegisterToVisits){
            String[] split = i.getvisitDay().split("-");
            if(split[2].equals(year)&&split[1].equals(month)&&split[0].equals(date)) result.add(i);
        }
        return result;
    }

    public String getCountVisit(){
        return String.valueOf( this.listRegisterToVisits.size());
    }
    public int getListQuy1(String year){
        int result = 0;
        for(RegisterToVisit i:listRegisterToVisits){
            String[] split = i.getvisitDay().split("-");
            if(split[2].equals(year)&&(split[1].equals("01")||split[1].equals("02")||split[1].equals("03"))){
                result++;
            }
        }
        return result;
    }
    public int getListQuy2(String year){
        int result = 0;
        for(RegisterToVisit i:listRegisterToVisits){
            String[] split = i.getvisitDay().split("-");
            if(split[2].equals(year)&&(split[1].equals("04")||split[1].equals("05")||split[1].equals("06"))){
                result++;
            }
        }
        return result;
    }
    public int getListQuy3(String year){
        int result = 0;
        for(RegisterToVisit i:listRegisterToVisits){
            String[] split = i.getvisitDay().split("-");
            if(split[2].equals(year)&&(split[1].equals("07")||split[1].equals("08")||split[1].equals("09"))){
                result++;
            }
        }
        return result;
    }
    public int getListQuy4(String year){
        int result = 0;
        for(RegisterToVisit i:listRegisterToVisits){
            String[] split = i.getvisitDay().split("-");
            if(split[2].equals(year)&&(split[1].equals("10")||split[1].equals("11")||split[1].equals("12"))){
                result++;
            }
        }
        return result;
    }

    public void setListRegisterToVisits(List<RegisterToVisit> listRegisterToVisits) {
        this.listRegisterToVisits = listRegisterToVisits;
    }
}

