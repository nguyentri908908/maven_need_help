package vn.viettuts.qlsv.func;
import vn.viettuts.qlsv.entity.Crime;
import vn.viettuts.qlsv.entity.Prison;
import vn.viettuts.qlsv.utils.FileUtils;

import java.text.Collator;
import java.util.*;

public class CimeFunc {
    private static final String Crime_FILE_NAME = "crime.xml";
    private List<Crime> listCrimes = this.readListCrimes();

    public CimeFunc() {
        this.listCrimes = readListCrimes();
        if(listCrimes == null){
            listCrimes = new ArrayList<Crime>();
        }
    }

    public void writeListCrimes(List<Crime> Crimes) {
        Prison prison = new Prison();
        prison.setCrime(Crimes);
        FileUtils.writeXMLtoFile(Crime_FILE_NAME, prison);
    }

    public List<Crime> readListCrimes() {
        List<Crime> list = new ArrayList();
        Prison prison = (Prison)FileUtils.readXMLFile(Crime_FILE_NAME, Prison.class);
        if (prison != null) {
            list = prison.getCrime();
        }

        return (List)list;
    }

    public void add(Crime Crime) {
        System.out.println("add");
        this.listCrimes.add(Crime);
        this.writeListCrimes(this.listCrimes);
    }

    public void edit(Crime Crime) {
        int size = this.listCrimes.size();
        for(int i = 0; i < size; ++i) {
            if (((Crime)this.listCrimes.get(i)).getIDInmate().equals( Crime.getIDInmate())) {
                ((Crime)this.listCrimes.get(i)).setName(Crime.getName());
                ((Crime)this.listCrimes.get(i)).setBirth(Crime.getBirth());
                ((Crime)this.listCrimes.get(i)).setGender(Crime.getGender());
                ((Crime)this.listCrimes.get(i)).setGuilty(Crime.getGuilty());
                ((Crime)this.listCrimes.get(i)).setRoomNumber(Crime.getRoomNumber());
                ((Crime)this.listCrimes.get(i)).setNumOfPrison(Crime.getNumOfPrison());
                System.out.println("ngu");
                this.writeListCrimes(this.listCrimes);
                break;
            }
        }

    }

    public boolean delete(Crime Crime) {
        boolean isFound = false;
        int size = this.listCrimes.size();

        for(int i = 0; i < size; ++i) {
            if (((Crime)this.listCrimes.get(i)).getIDInmate() == Crime.getIDInmate()) {
                Crime = (Crime)this.listCrimes.get(i);
                isFound = true;
                break;
            }
        }

        if (isFound) {
            this.listCrimes.remove(Crime);
            this.writeListCrimes(this.listCrimes);
            return true;
        } else {
            return false;
        }
    }

    public void sortCrimeByName() {
        Collections.sort(this.listCrimes, new Comparator<Crime>() {
            Collator collator = Collator.getInstance(new Locale("vi", "VN"));
            public int compare(Crime Crime1, Crime Crime2) {
                String[] parts1 = Crime1.getName().split(" ");
                String[] parts2 = Crime2.getName().split(" ");
                if(collator.compare ((parts1[parts1.length-1]),(parts2[parts2.length-1]))==0){
                    return collator.compare ((parts1[0]),(parts2[0]));
                }
                else
                return collator.compare ((parts1[parts1.length-1]),(parts2[parts2.length-1]));
            }
        });
    }
    public void sortCrimeById() {
        Collections.sort(this.listCrimes, new Comparator<Crime>() {
            public int compare(Crime Crime1, Crime Crime2) {
                return Crime1.getIDInmate().compareTo(Crime2.getIDInmate());
            }
        });
    }

    public void sortCrimeByYearInPrison() {
        Collections.sort(this.listCrimes, new Comparator<Crime>() {
            public int compare(Crime Crime1, Crime Crime2) {
                return Crime1.getNumOfPrison() > Crime2.getNumOfPrison() ? 1 : -1;
            }
        });
    }
    public boolean hasCrime(String id){
        for(Crime i: listCrimes){
            if(i.getIDInmate().equals(id)) return true;
        }
        return false;
    }
    public List<Crime> getListCrimes() {
        return this.listCrimes;
    }
    public List<Crime> getListCrimesByID(String Id){
        List<Crime> result = new ArrayList<>();
        for (Crime i: this.listCrimes) {

                if(i.getIDInmate().contains(Id) ){
                    result.add(i);
                    //System.out.println(Id);
                }
        }
        return result;
    }
    public List<Crime> getListCrimesByName(String Name){
        List<Crime> result = new ArrayList<>();
        for (Crime i: this.listCrimes) {
            if(i.getName().contains(Name) ){
                result.add(i);
            }
        }
        return result;
    }
    public List<Crime> getListCrimesByBirth(String Birth){
        List<Crime> result = new ArrayList<>();
        for (Crime i: this.listCrimes) {
            if(i.getBirth().contains(Birth) ){
                result.add(i);
            }
        }
        return result;
    }
    public List<Crime> getListCrimesByGender(String Gen){
        List<Crime> result = new ArrayList<>();
        for (Crime i: this.listCrimes) {
            if(i.getGender().contains(Gen)){
                result.add(i);
            }
        }
        return result;
    }
    public String getCountCrime(){
        return String.valueOf( this.listCrimes.size());
    }
    public void setListCrimes(List<Crime> listCrimes) {
        this.listCrimes = listCrimes;
    }
}

