package vn.viettuts.qlsv.entity;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.time.*;
import java.util.Date;
@XmlRootElement(
        name = "RegisterToVisit"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class RegisterToVisit implements Serializable {
    private static final long serialVersionUID = 1L;
    private int Id;
    private String IdPrison;
    private String visitDay;
    private int timeStart;
    private int hour;
    private String visitLocation;
    private String NameOfVisiter;
    private String Relasionship;

    public RegisterToVisit(){

    }
    public RegisterToVisit(int Id,String prisoner, String visitDay, int timeStart,int hour, String visitLocation, String NameOfVisitter,String Relasionship) {
        this.Id = Id;
        this.IdPrison = prisoner;
        this.visitDay = visitDay;
        this.timeStart = timeStart;
        this.hour = hour;
        this.visitLocation = visitLocation;
        this.NameOfVisiter = NameOfVisitter;
        this.Relasionship = Relasionship;
    }

    // Các phương thức getter và setter cho các thuộc tính của đăng ký thăm nuôi
    public String getPrisoner() {
        return IdPrison;
    }
    public int getId(){
        return Id;
    }
    public void setId(int Id){
        this.Id=Id;
    }
    public void setPrisoner(String prisoner) {
        this.IdPrison = prisoner;
    }

    public String getvisitDay() {
        return visitDay;
    }

    public void setvisitDay(String visitDay) {
        this.visitDay = visitDay;
    }

    public int gettimeStart() {
        return timeStart;
    }

    public void settimeStart(int timeStart) {
        this.timeStart = timeStart;
    }
    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public String getVisitLocation() {
        return visitLocation;
    }

    public void setVisitLocation(String visitLocation) {
        this.visitLocation = visitLocation;
    }

    public String getNameOfVisiter(){
        return NameOfVisiter;
    }
    public void setNameOfVisiter(String NameOfVisitter){
        this.NameOfVisiter=NameOfVisitter;
    }
    public String getRelasionship(){
        return Relasionship;
    }
    public void setRelasionship(String Relasionship){
        this.Relasionship = Relasionship;
    }
}