package vn.viettuts.qlsv.entity;
import vn.viettuts.qlsv.view.LoginView;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAttribute;
import java.time.*;
@XmlRootElement(
        name = "crime"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class Crime implements Serializable {
    private static final long serialVersionUID = 1L;
    private String IDInmate;

    private String Name;
    //@XmlAttribute(name = "Birth")
    private String Birth;
    //@XmlAttribute(name = "Gender")
    private String Gender;
    //@XmlAttribute(name = "Guilty")
    private String Guilty;
    //@XmlAttribute(name = "NumOfPrison")
    private int NumOfPrison;
    //@XmlAttribute(name = "RoomNumber")
    private int RoomNumber;
    public Crime(){

    }
    
    public Crime(String IDInmate, String Name, String Birth, String Gender, String Guilty, int NumOfPrison, int RoomNumber){
        this.IDInmate = IDInmate;
        this.Name = Name;
        this.Birth= Birth;
        this.Gender = Gender;
        this.Guilty = Guilty;
        this.NumOfPrison = NumOfPrison;
        this.RoomNumber = RoomNumber;

    }
    /*
    ===========
    get, set số hiệu phạm nhân
    ===========
     */
    public String getIDInmate() {
        return IDInmate;
    }

    public void setIDInmate(String IDInmate) {
        this.IDInmate = IDInmate;
    }
    /*
    =============
    get, set tên phạm nhân
    =============
     */
    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }
    /*
    ==============
    get, set ngày sinh
    ==============
    */
    public String getBirth() {
        return Birth;
    }

    public void setBirth(String Birth) {
        this.Birth = Birth;
    }

    /*
    ==================
    get, set giới tính
    ==================
     */
    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    /*
    ==================
    get, set tội danh
    ==================
     */
    public String getGuilty() {
        return Guilty;
    }

    public void setGuilty(String Guilty) {
        this.Guilty = Guilty;
    }

    /*
    ==========================
    get, set số năm trong tù
    =========================
     */
    public int getNumOfPrison() {
        return NumOfPrison;
    }

    public void setNumOfPrison(int NumOfPrison) {
        this.NumOfPrison = NumOfPrison;
    }

    /*
    =======================
    get, set số phòng giam
    =======================
     */
    public int getRoomNumber() {
        return RoomNumber;
    }

    public void setRoomNumber(int RoomNumber) {
        this.RoomNumber = RoomNumber;
    }
    
}
