package vn.viettuts.qlsv.entity;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "crimes")
@XmlAccessorType(XmlAccessType.FIELD)
public class Prison {
//    private String Name = "Cyber 14B Prison";
//    /*
//    -------------------------
//    Số lượng tội phạm tối đa
//    --------------------------
//     */
//    private int MaxCiminal = 1000000;
//    /*
//    --------------------
//    Số lượng phòng giam
//    --------------------
//     */
//    private int NumberOfCells = 500000;
    private List<Crime> crime;



    public List<Crime> getCrime() {
        return this.crime;
    }

    public void setCrime(List<Crime> crime) {
        this.crime = crime;
    }
//    public String  getName(){
//        return this.Name;
//    }
//    public int getMaxCiminal(){
//        return this.MaxCiminal;
//    }
//    public int getNumberOfCells(){
//        return this.NumberOfCells;
//    }
}
