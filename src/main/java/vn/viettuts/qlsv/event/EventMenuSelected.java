package vn.viettuts.qlsv.event;

public interface EventMenuSelected {

    public void selected(int index);
}
