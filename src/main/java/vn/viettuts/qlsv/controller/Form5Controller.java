package vn.viettuts.qlsv.controller;

import vn.viettuts.qlsv.entity.Crime;
import vn.viettuts.qlsv.entity.RegisterToVisit;
import vn.viettuts.qlsv.form.Form5;
import vn.viettuts.qlsv.form.FrameEditCrime;
import vn.viettuts.qlsv.form.FrameEditVisit;
import vn.viettuts.qlsv.func.CimeFunc;
import vn.viettuts.qlsv.func.VisitFunc;
import vn.viettuts.qlsv.func.VisitFunc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class Form5Controller {
    private VisitFunc visitfunc;
    private Form5 view;
    private FrameEditVisit frame;
    public Form5Controller(Form5 view){
        this.view=view;
        visitfunc = new VisitFunc();
        view.addDeleteVisitListener(new DeleteVisitListener());
        view.addEditVisitListener(new EditVisitListener());
        view.addSearchVisitListener(new SearchVisitListener());
        view.addResetListener(new ResetVisitListener());

    }
    public void showForm5(){
        List<RegisterToVisit> crimeList = visitfunc.getListRegisterToVisits();
        view.showVisit(crimeList);
    }
    public void ResetListTable(){
        visitfunc = new VisitFunc();
        view.showVisit(visitfunc.getListRegisterToVisits());
    }
    class DeleteVisitListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            RegisterToVisit visit = view.getVisitInfo();
            if (visit != null) {
                visitfunc.delete(visit);
                //view.clearCrimeInfo();
                view.showVisit(visitfunc.getListRegisterToVisits());
                view.showMessage("Xóa thành công!");
            }
        }
    }
    class SearchVisitListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int index = view.getIndexSearch();
            String infor = view.getFieldSearch().trim();
            System.out.println(infor);
            if(index==0){
                visitfunc = new VisitFunc();
                view.showVisit(visitfunc.getListVisitById(Integer.parseInt(infor)));
            }
            else if(index==1){
                view.showVisit(visitfunc.getListVisitByIdPrison(infor));
            } else if(index==2){
                view.showVisit(visitfunc.getListVisitByVisitDay(infor));
            } else if(index==3){
                view.showVisit(visitfunc.getListVisitByLocation(infor));
            } else if (index==4) {
                view.showVisit(visitfunc.getListVisitByNameVisiter(infor));
            }else if (index==5) {
                view.showVisit(visitfunc.getListVisitByRelasionship(infor));
            }

        }
    }
    class ResetVisitListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            visitfunc = new VisitFunc();
            view.showVisit(visitfunc.getListRegisterToVisits());

        }
    }
    class EditVisitListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            RegisterToVisit visit = view.getVisitInfo();
            if (visit != null) {
                //visitfunc.edit(crime);
                frame = new FrameEditVisit(visit);
                EditVisitController controller = new EditVisitController(frame,view);
                controller.showFrame();
//                CrimeView.showCrime(Crime);
//                CrimeView.showListCrimes(CrimeDao.getListCrimes());
//                CrimeView.showMessage("Cập nhật thành công!");
            }
        }
    }
}
