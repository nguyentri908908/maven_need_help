package vn.viettuts.qlsv.controller;

import vn.viettuts.qlsv.entity.Crime;
import vn.viettuts.qlsv.entity.Crime;
import vn.viettuts.qlsv.entity.Crime;
import vn.viettuts.qlsv.form.Form3;
import vn.viettuts.qlsv.form.FrameEditCrime;
import vn.viettuts.qlsv.func.CimeFunc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class Form3Controller {
    private CimeFunc crimeFunc;
    private Form3 view;
    private FrameEditCrime frame;
    public Form3Controller(Form3 view){
        this.view=view;
        crimeFunc = new CimeFunc();
        view.addDeletecrimeListener(new DeleteCrimeListener());
        view.addSearchcrimeListener(new SearchCrimeListener());
        view.addEditrimeListener(new EditCrimeListener());

    }
    public void showForm3(){
        List<Crime> crimeList = crimeFunc.getListCrimes();
        view.showListCrimes(crimeList);
    }
    public void ResetListTable(){
        crimeFunc = new CimeFunc();
        view.showListCrimes(crimeFunc.getListCrimes());
    }
    class DeleteCrimeListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            Crime Crime = view.getCrimeInfo();
            if (Crime != null) {
                crimeFunc.delete(Crime);
                //view.clearCrimeInfo();
                view.showListCrimes(crimeFunc.getListCrimes());
                view.showMessage("Xóa thành công!");
            }
        }
    }
    class SearchCrimeListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int index = view.getIndexSearch();
            String infor = view.getFieldSearch().trim();
            System.out.println(infor);
            if(index==0){
                crimeFunc = new CimeFunc();
                view.showListCrimes(crimeFunc.getListCrimes());
            }
            else if(index==1){
                view.showListCrimes(crimeFunc.getListCrimesByID(infor));
            } else if(index==2){
                view.showListCrimes(crimeFunc.getListCrimesByName(infor));
            } else if(index==3){
                view.showListCrimes(crimeFunc.getListCrimesByGender(infor));
            } else if (index==4) {
                view.showListCrimes(crimeFunc.getListCrimesByBirth(infor));
            }

        }
    }
    class EditCrimeListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            Crime crime = view.getCrimeInfo();
            if (crime != null) {
                //crimeFunc.edit(crime);
                frame = new FrameEditCrime(crime);
                EditCrimeController controller = new EditCrimeController(frame,view);
                controller.showFrame();
//                CrimeView.showCrime(Crime);
//                CrimeView.showListCrimes(CrimeDao.getListCrimes());
//                CrimeView.showMessage("Cập nhật thành công!");
            }
        }
    }

}
