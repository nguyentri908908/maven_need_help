package vn.viettuts.qlsv.controller;

import vn.viettuts.qlsv.entity.Crime;
import vn.viettuts.qlsv.form.Form3;
import vn.viettuts.qlsv.form.FrameEditCrime;
import vn.viettuts.qlsv.func.CimeFunc;
import vn.viettuts.qlsv.view.DashBoardView;
import vn.viettuts.qlsv.view.LoginView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EditCrimeController {
    private FrameEditCrime frame;
    private CimeFunc crimeFunc;
    private Form3Controller controllfor3;
    private Form3 form3;

    public EditCrimeController(FrameEditCrime frame, Form3 form3){
        this.frame=frame;
        crimeFunc = new CimeFunc();
        frame.addListennerEdit(new EditListener());
        this.form3 = form3;
    }
    public void showFrame(){
        frame.setVisible(true);
        frame.setDefaultCloseOperation(FrameEditCrime.DISPOSE_ON_CLOSE);
    }
    class EditListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            Crime crime = frame.getCrimeInfo();

            if (crime != null) {

                crimeFunc.edit(crime);
                controllfor3 = new Form3Controller(form3);
                frame.setVisible(false);
                crimeFunc = new CimeFunc();
                controllfor3.ResetListTable();
            }
        }
    }

}
