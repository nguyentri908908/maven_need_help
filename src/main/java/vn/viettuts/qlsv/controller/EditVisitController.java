package vn.viettuts.qlsv.controller;

import vn.viettuts.qlsv.entity.Crime;
import vn.viettuts.qlsv.entity.RegisterToVisit;
import vn.viettuts.qlsv.form.Form5;
import vn.viettuts.qlsv.form.FrameEditVisit;
import vn.viettuts.qlsv.func.CimeFunc;
import vn.viettuts.qlsv.func.VisitFunc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EditVisitController {
    private FrameEditVisit frame;
    private VisitFunc visitFunc;
    private Form5 form5;
    private Form5Controller form5controller;


    public EditVisitController(FrameEditVisit frame, Form5 form5){
        this.frame=frame;
        visitFunc = new VisitFunc();
        frame.addListennerEdit(new EditListener());
        this.form5 = form5;
    }
    public void showFrame(){
        frame.setVisible(true);
        frame.setDefaultCloseOperation(FrameEditVisit.DISPOSE_ON_CLOSE);
    }
    class EditListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            RegisterToVisit visit = frame.getCrimeInfo();

            if (visit != null) {

                visitFunc.edit(visit);
                frame.setVisible(false);
                form5controller =  new Form5Controller(form5);
                form5controller.ResetListTable();
            }
        }
    }


}
