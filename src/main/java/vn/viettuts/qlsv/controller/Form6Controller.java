package vn.viettuts.qlsv.controller;

import vn.viettuts.qlsv.entity.Crime;
import vn.viettuts.qlsv.entity.RegisterToVisit;
import vn.viettuts.qlsv.form.Form5;
import vn.viettuts.qlsv.form.Form6;
import vn.viettuts.qlsv.func.CimeFunc;
import vn.viettuts.qlsv.func.VisitFunc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class Form6Controller {
    private VisitFunc visitfunc;
    private CimeFunc crimefunc;
    private Form6 view;
    public Form6Controller(Form6 view){
        this.view=view;
        visitfunc = new VisitFunc();
        crimefunc = new CimeFunc();
        view.addSortCrimeListener(new SortCrimeListener());
        view.addFilterVisitListener(new FilterVisitListener());
//        view.addDeleteVisitListener(new Form5Controller.DeleteVisitListener());
//        view.addEditVisitListener(new Form5Controller.EditVisitListener());
//        view.addSearchVisitListener(new Form5Controller.SearchVisitListener());
//        view.addResetListener(new Form5Controller.ResetVisitListener());

    }
    public void showForm6(){
        List<RegisterToVisit> visitList = visitfunc.getListRegisterToVisits();
        List<Crime> crimeList = crimefunc.getListCrimes();
        view.showVisit(visitList);
        view.showListCrimes(crimeList);
    }

    class SortCrimeListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
           int index = view.getIndexSort();
           if(index==0){
               crimefunc.sortCrimeById();
               List<Crime> crimeList = crimefunc.getListCrimes();
               view.showListCrimes(crimeList);
           }
            if(index==1){
                crimefunc.sortCrimeByName();
                List<Crime> crimeList = crimefunc.getListCrimes();
                view.showListCrimes(crimeList);
            }
            if(index==2){
                crimefunc.sortCrimeByYearInPrison();
                List<Crime> crimeList = crimefunc.getListCrimes();
                view.showListCrimes(crimeList);
            }
        }
    }
    private boolean validateDate(String month, int date) {

        if((month.equals("01")||month.equals("03")||month.equals("05")||month.equals("07")||month.equals("08")
                ||month.equals("10")||month.equals("12")) && (date<1||date>31)){
            view.showMessage("Ngày không tồn tại trong tháng");
            return false;
        }
        if(month.equals("02") && (date<1||date>29)){
            view.showMessage("Ngày không tồn tại trong tháng");
            return false;
        }
        if((month.equals("04")||month.equals("06")||month.equals("09")||month.equals("11")) && (date<1||date>30)){

            view.showMessage("Ngày không tồn tại trong tháng");
            return false;
        }

        return true;
    }
    class FilterVisitListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String year = view.getYearFiel();
            String month = view.getMonthFiel();
            String day = view.getDayFiel();
            System.out.println(year+"  "+month+"  "+day);
          
            if(year!=null && (month==null||month.equals("")) && (day==null||day.equals(""))){
                
                view.showVisit(visitfunc.getListFilterYear(year));
            }
            else if(year!=null && month!=null && (day==null||day.equals(""))){
                
                view.showVisit(visitfunc.getListFilterMonth(year,month));
            }
           else if(year!=null && month!=null && day!=null){
               if(validateDate(month,Integer.parseInt(day)))
                view.showVisit(visitfunc.getListFilterDate(year,month,day));
            }
        }
    }
}
