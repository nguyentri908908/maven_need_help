package vn.viettuts.qlsv.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import vn.viettuts.qlsv.entity.Crime;
import vn.viettuts.qlsv.form.Form2;
import vn.viettuts.qlsv.func.CimeFunc;
public class Form2Controller {

    private CimeFunc cimeFunc;
    private Form2 view;

    public Form2Controller(Form2 view) {
        this.view = view;
        cimeFunc = new CimeFunc();

        view.addAddCrimeListener(new AddCrimeListener());
        //System.out.println("controll");

    }

    class AddCrimeListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            System.out.println("action");
            Crime crime = view.getCrimeInfo();
            if (crime != null) {
                cimeFunc.add(crime);
                //c.showStudent(student);
                //studentView.showListStudents(studentDao.getListStudents());
                view.showMessage("Thêm thành công!");
                view.resetField();
            }
        }
    }
    public void showForm2(){

    }
}
