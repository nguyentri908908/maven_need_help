package vn.viettuts.qlsv.controller;

import vn.viettuts.qlsv.entity.RegisterToVisit;
import vn.viettuts.qlsv.entity.RegisterToVisit;
import vn.viettuts.qlsv.form.Form4;
import vn.viettuts.qlsv.func.CimeFunc;
import vn.viettuts.qlsv.func.VisitFunc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Form4Controller {
    private VisitFunc visitFunc;
    private Form4 view;

    public Form4Controller(Form4 view) {
        this.view = view;
        visitFunc = new VisitFunc();

        view.addAddVisitListener(new Form4Controller.AddVisitListener());
        //System.out.println("controll");

    }

    class AddVisitListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            System.out.println("action");
            RegisterToVisit visit = view.getVisitInfo();
            if (visit != null) {
                visitFunc.add(visit);
                //c.showStudent(student);
                //studentView.showListStudents(studentDao.getListStudents());
                view.showMessage("Thêm thành công!");
                view.resetField();
            }
        }
    }
    public void showForm4(){

    }
}
