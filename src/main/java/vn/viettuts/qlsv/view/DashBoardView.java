/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.viettuts.qlsv.view;

/**
 *
 * @author Nguyen Duc Tri
 */

import vn.viettuts.qlsv.swing.component.Menu;
import vn.viettuts.qlsv.controller.*;
import vn.viettuts.qlsv.event.EventMenuSelected;
import vn.viettuts.qlsv.form.*;
import vn.viettuts.qlsv.swing.model.ModelMenu;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.animation.timing.Animator;
import org.jdesktop.animation.timing.TimingTarget;
import org.jdesktop.animation.timing.TimingTargetAdapter;

public class DashBoardView extends javax.swing.JFrame {

    private Menu menu = new Menu();
    private JPanel main = new JPanel();
    private MigLayout layout;
    private Animator animator;
    private boolean menuShow;

    public DashBoardView() {
        initComponents();
        init();
    }

    private void init() {
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        layout = new MigLayout("fill", "0[]10[]5", "0[fill]0");
        body.setLayout(layout);
        main.setOpaque(false);
        main.setLayout(new BorderLayout());
        menu.addEventMenu(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if (!animator.isRunning()) {
                    animator.start();
                }
            }
        });
        menu.setEvent(new EventMenuSelected() {
            @Override
            public void selected(int index) {
                if (index == 0) {
                    showForm(new Form1());
                } else if (index == 1) {
                    Form2 form2 = new Form2();
                    Form2Controller controller = new Form2Controller(form2);
                    controller.showForm2();
                    showForm(form2);
                } else if (index==2) {
                    Form3 form3 = new Form3();
                    Form3Controller controller = new Form3Controller(form3);
                    controller.showForm3();
                    showForm(form3);
                } else if (index==3) {
                    Form4 form4 = new Form4();
                    Form4Controller controller = new Form4Controller(form4);
                    controller.showForm4();
                    showForm(form4);
                } else if (index==4) {
                    Form5 form5 = new Form5();
                    Form5Controller controller = new Form5Controller(form5);
                    controller.showForm5();
                    showForm(form5);
                } else if (index==5){
                    Form6 form6 = new Form6();
                    Form6Controller controller = new Form6Controller(form6);
                    controller.showForm6();
                    showForm(form6);
                }
            }
        });

        menu.addMenu(new ModelMenu("Tổng quan", new ImageIcon(getClass().getResource("/vn/viettuts/qlsv/icon/home.png"))));
        menu.addMenu(new ModelMenu("Thêm phạm nhân", new ImageIcon(getClass().getResource("/vn/viettuts/qlsv/icon/add.png"))));
        menu.addMenu(new ModelMenu("Quản lý phạm nhân", new ImageIcon(getClass().getResource("/vn/viettuts/qlsv/icon/managecrime.png"))));
        menu.addMenu(new ModelMenu("Thêm lịch thăm nuôi", new ImageIcon(getClass().getResource("/vn/viettuts/qlsv/icon/addlist.png"))));
        menu.addMenu(new ModelMenu("Quản lý lịch thăm", new ImageIcon(getClass().getResource("/vn/viettuts/qlsv/icon/managedetail.png"))));
        menu.addMenu(new ModelMenu("Xuất file excel", new ImageIcon(getClass().getResource("/vn/viettuts/qlsv/icon/export.png"))));
        body.add(menu, "w 50!");
        body.add(main, "w 100%");
        TimingTarget target = new TimingTargetAdapter() {
            @Override
            public void timingEvent(float fraction) {
                double width;
                if (menuShow) {
                    width = 50 + (150 * (1f - fraction));
                    menu.setAlpha(1f - fraction);
                } else {
                    width = 50 + (150 * fraction);
                    menu.setAlpha(fraction);
                }
                layout.setComponentConstraints(menu, "w " + width + "!");
                body.revalidate();
            }

            @Override
            public void end() {
                menuShow = !menuShow;
            }
        };
        animator = new Animator(400, target);
        animator.setResolution(0);
        animator.setAcceleration(0.5f);
        animator.setDeceleration(0.5f);
        showForm(new Form1());
    }

    private void showForm(Component com) {
        main.removeAll();
        main.add(com);
        main.repaint();
        main.revalidate();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        body = new JPanel();


        setUndecorated(true);

        body.setBackground(new java.awt.Color(245, 245, 245));

        javax.swing.GroupLayout bodyLayout = new javax.swing.GroupLayout(body);
        body.setLayout(bodyLayout);
        bodyLayout.setHorizontalGroup(
                bodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1131, Short.MAX_VALUE)
        );
        bodyLayout.setVerticalGroup(
                bodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 653, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(body, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(body, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>                        
    public Menu getMenu(){
        return this.menu;
    }
    public void addExitListener(ActionListener listener) {
        menu.addEventLogout(listener);
    }

    // Variables declaration - do not modify                     
    private JPanel body;
    // End of variables declaration                   
}

