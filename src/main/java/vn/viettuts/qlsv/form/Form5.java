/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package vn.viettuts.qlsv.form;

import vn.viettuts.qlsv.entity.Crime;
import vn.viettuts.qlsv.entity.RegisterToVisit;
import vn.viettuts.qlsv.func.CimeFunc;

import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.List;

/**
 *
 * @author Nguyen Duc Tri
 */
public class Form5 extends javax.swing.JPanel {

    public Form5() {
        initComponents();
        setOpaque(false);
        searchList.addElement("Id");
        searchList.addElement("Mã phạm nhân");
        searchList.addElement("Ngày thăm");
        searchList.addElement("Khu vực");
        searchList.addElement("Tên người thăm");
        searchList.addElement("Mối quan hệ");
        boxChoose.setModel(searchList);
        boxChoose.setSelectedIndex(-1);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtSearch = new vn.viettuts.qlsv.swing.TextFieldAnimation();
        boxChoose = new vn.viettuts.qlsv.swing.Combobox();
        buttonEdit = new vn.viettuts.qlsv.swing.ButtonForm();
        buttonDelete = new vn.viettuts.qlsv.swing.ButtonForm();
        buttonReset = new vn.viettuts.qlsv.swing.ButtonForm();
        scrollBar1 = new vn.viettuts.qlsv.swing.ScrollBar();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableColumn1 = new vn.viettuts.qlsv.swing.TableColumn();

        setBackground(new java.awt.Color(230, 230, 230));

        jPanel1.setBackground(new java.awt.Color(230, 230, 230));

        jLabel1.setFont(new java.awt.Font("Segoe UI Black", 1, 19)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(153, 51, 0));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("DANH SÁCH ĐĂNG KÍ THĂM NUÔI");

        txtSearch.setAnimationColor(new java.awt.Color(204, 102, 0));
        txtSearch.setSelectionColor(new java.awt.Color(204, 51, 0));

        boxChoose.setLabeText("Chọn trường");
        boxChoose.setLineColor(new java.awt.Color(204, 102, 0));

        buttonEdit.setBackground(new java.awt.Color(255, 102, 0));
        buttonEdit.setForeground(new java.awt.Color(255, 255, 255));
        buttonEdit.setText("Sửa");

        buttonDelete.setBackground(new java.awt.Color(255, 0, 0));
        buttonDelete.setForeground(new java.awt.Color(255, 255, 255));
        buttonDelete.setText("Xóa");

        buttonReset.setBackground(new java.awt.Color(102, 102, 255));
        buttonReset.setForeground(new java.awt.Color(255, 255, 255));
        buttonReset.setText("Lầm mới");
        buttonReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonResetActionPerformed(evt);
            }
        });

        tableColumn1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrollBar1.setBackground(new java.awt.Color(139, 229, 222));
        jScrollPane1.setViewportView(tableColumn1);
        jScrollPane1.setVerticalScrollBar(scrollBar1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 479, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(boxChoose, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(buttonEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(buttonDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(buttonReset, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(126, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(boxChoose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(buttonEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(buttonDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(buttonReset, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
                .addGap(24, 24, 24))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(112, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    public void addDeleteVisitListener(ActionListener listener) {
        buttonDelete.addActionListener(listener);
    }
    public void addSearchVisitListener(ActionListener listener) {

        txtSearch.addActionListener(listener);
    }
    public void addEditVisitListener(ActionListener listener) {

        buttonEdit.addActionListener(listener);
    }
    public void addResetListener(ActionListener listener){
        buttonReset.addActionListener(listener);
    }
    public int getIndexSearch(){
        return boxChoose.getSelectedIndex();
    }
    public String getFieldSearch(){
        return txtSearch.getText();
    }
    public void showMessage(String message) {
        JOptionPane.showMessageDialog(this, message);
    }
    public void showVisit(List<RegisterToVisit> list) {
        int size = list.size();
        // với bảng studentTable có 5 cột, 
        // khởi tạo mảng 2 chiều visits, trong đó:
        // số hàng: là kích thước của list student 
        // số cột: là 5
        Object [][] visits = new Object[size][8];
        for (int i = 0; i < size; i++) {
            visits[i][0] = list.get(i).getId();
            visits[i][1] = list.get(i).getPrisoner();
            //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            visits[i][2] = list.get(i).getvisitDay();
            visits[i][3] = list.get(i).gettimeStart();
            visits[i][4] = list.get(i).getHour();
            visits[i][5] = list.get(i).getVisitLocation();
            visits[i][6] = list.get(i).getNameOfVisiter();
            visits[i][7] = list.get(i).getRelasionship();
        }
        tableColumn1.setModel(new DefaultTableModel(visits, columnNames));
    }
    public RegisterToVisit getVisitInfo() {
        // validate student
        int row = tableColumn1.getSelectedRow();

        int Id = Integer.parseInt( tableColumn1.getModel().getValueAt(row,0).toString());
        String IdCrime = tableColumn1.getModel().getValueAt(row,1).toString();
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String Date = tableColumn1.getModel().getValueAt(row,2).toString();
        //LocalDateTime Date = LocalDateTime.parse(DateString,formatter);
        int Start = Integer.parseInt( tableColumn1.getModel().getValueAt(row,3).toString());
        int Hour = Integer.parseInt( tableColumn1.getModel().getValueAt(row,4).toString());
        String Location = tableColumn1.getModel().getValueAt(row,5).toString();
        String NameVisit = tableColumn1.getModel().getValueAt(row,6).toString();
        String Relasionship = tableColumn1.getModel().getValueAt(row,7).toString();
        return new RegisterToVisit(Id,IdCrime,Date,Start,Hour,Location,NameVisit,Relasionship);
    }
    private DefaultComboBoxModel searchList = new DefaultComboBoxModel();
    private final String [] columnNames = new String [] {
            "ID", "ID phạm nhân", "Ngày thăm", "Bắt đầu (giờ)", "Số giờ","Địa điểm","Người thăm", "Quan hệ"};
    private void buttonResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonResetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_buttonResetActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private vn.viettuts.qlsv.swing.ScrollBar scrollBar1;
    private vn.viettuts.qlsv.swing.Combobox boxChoose;
    private vn.viettuts.qlsv.swing.ButtonForm buttonDelete;
    private vn.viettuts.qlsv.swing.ButtonForm buttonEdit;
    private vn.viettuts.qlsv.swing.ButtonForm buttonReset;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private vn.viettuts.qlsv.swing.TableColumn tableColumn1;
    private vn.viettuts.qlsv.swing.TextFieldAnimation txtSearch;
    // End of variables declaration//GEN-END:variables
}