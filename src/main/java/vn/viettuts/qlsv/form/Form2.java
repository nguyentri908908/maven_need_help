/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package vn.viettuts.qlsv.form;

import vn.viettuts.qlsv.entity.Crime;
import vn.viettuts.qlsv.func.CimeFunc;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 *
 * @author Nguyen Duc Tri
 */
public class Form2 extends javax.swing.JPanel {

    /**
     * Creates new form Form2
     */
    public Form2() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtID = new vn.viettuts.qlsv.swing.TextFielInForm();
        txtName = new vn.viettuts.qlsv.swing.TextFielInForm();
        txtBirth = new vn.viettuts.qlsv.swing.TextFielInForm();
        genderList.addElement("Nam");
        genderList.addElement("Nữ");
        genderList.addElement("Khác");
        boxGenger = new vn.viettuts.qlsv.swing.Combobox();
        boxGenger.setModel(genderList);
        boxGenger.setSelectedIndex(-1);
        txtGuility = new vn.viettuts.qlsv.swing.TextFielInForm();
        txtYear = new vn.viettuts.qlsv.swing.TextFielInForm();
        txtRoom = new vn.viettuts.qlsv.swing.TextFielInForm();
        buttonForm1 = new vn.viettuts.qlsv.swing.ButtonForm();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(212, 212, 212));
        setForeground(new java.awt.Color(204, 204, 204));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        txtID.setLabelText("ID");
        txtID.setLineColor(new java.awt.Color(204, 153, 0));
        txtID.setSelectionColor(new java.awt.Color(204, 204, 0));

        txtName.setLabelText("Tên");
        txtName.setLineColor(new java.awt.Color(204, 153, 0));
        txtName.setSelectionColor(new java.awt.Color(204, 204, 0));

        txtBirth.setLabelText("Ngày sinh");
        txtBirth.setLineColor(new java.awt.Color(204, 153, 0));
        txtBirth.setSelectionColor(new java.awt.Color(204, 204, 0));

        boxGenger.setLabeText("Giới tính");
        boxGenger.setLineColor(new java.awt.Color(153, 153, 0));

        txtGuility.setLabelText("Tội danh");
        txtGuility.setLineColor(new java.awt.Color(204, 153, 0));
        txtGuility.setSelectionColor(new java.awt.Color(204, 204, 0));

        txtYear.setLabelText("Số năm tù");
        txtYear.setLineColor(new java.awt.Color(204, 153, 0));
        txtYear.setSelectionColor(new java.awt.Color(204, 204, 0));

        txtRoom.setLabelText("Số phòng");
        txtRoom.setLineColor(new java.awt.Color(204, 153, 0));
        txtRoom.setSelectionColor(new java.awt.Color(204, 204, 0));

        buttonForm1.setBackground(new java.awt.Color(204, 153, 0));
        buttonForm1.setForeground(new java.awt.Color(255, 255, 255));
        buttonForm1.setText("Xong");

        jLabel1.setFont(new java.awt.Font("SansSerif", 1, 19)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 102, 0));
        jLabel1.setLabelFor(jPanel1);
        jLabel1.setText("NHẬP THÔNG TIN PHẠM NHÂN");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(135, 135, 135)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtBirth, javax.swing.GroupLayout.PREFERRED_SIZE, 297, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(boxGenger, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(63, 63, 63)
                                .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 365, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtGuility, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtYear, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtRoom, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(337, 337, 337)
                        .addComponent(buttonForm1, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(151, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 291, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(268, 268, 268))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBirth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(boxGenger, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addComponent(txtGuility, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtRoom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addComponent(buttonForm1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(13, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(114, 114, 114)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(92, 92, 92))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(23, 23, 23))
        );
    }// </editor-fold>//GEN-END:initComponents

    public void addAddCrimeListener(ActionListener listener) {
        System.out.println("do");
        buttonForm1.addActionListener(listener);
    }
    public void showMessage(String message) {
        JOptionPane.showMessageDialog(this, message);
    }

    private boolean validateName() {
        String name = txtName.getText();
        if (name == null || "".equals(name.trim())) {
            txtName.requestFocus();
            showMessage("Name không được trống.");
            return false;
        }
        return true;
    }
    private boolean validateID() {
        String id = txtID.getText();
        if (id == null || "".equals(id.trim())) {
            txtID.requestFocus();
            showMessage("ID không được trống.");
            return false;
        }
        CimeFunc func = new CimeFunc();
        if(func.hasCrime(id)){
            txtID.requestFocus();
            showMessage("Đã tồn tại Id tội phạm, vui lòng nhập Id khác");
            return false;
        }
        return true;
    }
    private boolean validateBirth() {
        String birth = txtBirth.getText();
        if (birth == null || "".equals(birth.trim())) {
            txtBirth.requestFocus();
            showMessage("Birth không được trống.");
            return false;
        }
        return true;
    }
    private boolean validateGuility() {
        String guility = txtGuility.getText();
        if (guility == null || "".equals(guility.trim())) {
            txtName.requestFocus();
            showMessage("Tội danh không được trống.");
            return false;
        }
        return true;
    }
    private boolean validateGender() {
        int gen = boxGenger.getSelectedIndex();
        if (gen==-1) {
            boxGenger.requestFocus();
            showMessage("Chưa chọn giới tính");
            return false;
        }
        return true;
    }
    private boolean validateRoom() {
        try {
            Integer room = Integer.parseInt(txtRoom.getText());
            if (room < 0 || room > 1000) {
                txtRoom.requestFocus();
                showMessage("Phòng không hợp lệ, phòng trong khoảng 0 đến 1000.");
                return false;
            }
        } catch (Exception e) {
            txtRoom.requestFocus();
            showMessage("Phong không hợp lệ!");
            return false;
        }
        return true;
    }
    private boolean validateYear() {
        try {
            Integer year = Integer.parseInt (txtYear.getText());
            if (year < 0 || year > 100) {
                txtYear.requestFocus();
                showMessage("Số năm tù không hợp lệ nên trong khoảng 0 đến 100.");
                return false;
            }
        } catch (Exception e) {
            txtYear.requestFocus();
            showMessage("Year không hợp lệ!");
            return false;
        }
        return true;
    }
    public void setCrimeInfo(Crime crime){
        txtID.setText(crime.getIDInmate());
        txtName.setText(crime.getName());
        txtYear.setText( String.valueOf(crime.getNumOfPrison()) );
        txtBirth.setText(crime.getBirth());
        txtGuility.setText(crime.getGuilty());
        txtRoom.setText(String.valueOf(crime.getRoomNumber()));
        if(crime.getGender().equals("Nam")){
            boxGenger.setSelectedIndex(0);
        }
        if(crime.getGender().equals("Nữ")){
            boxGenger.setSelectedIndex(1);
        }
        if(crime.getGender().equals("Khác")){
            boxGenger.setSelectedIndex(2);
        }
    }
    public Crime getCrimeInfo() {
        // validate student
        if (!validateName() || !validateBirth() || !validateGender() || !validateID()||!validateGuility()||!validateRoom()||!validateYear()) {
            return null;
        }
        try {
            Crime crime = new Crime();


            crime.setIDInmate((txtID.getText()));
            crime.setName(txtName.getText().trim());
            crime.setBirth(txtBirth.getText().trim());
            crime.setGender(boxGenger.getItemAt( boxGenger.getSelectedIndex()).toString());
            crime.setGuilty(txtGuility.getText());
            crime.setRoomNumber(Integer.parseInt(txtRoom.getText()));
            crime.setNumOfPrison(Integer.parseInt(txtYear.getText()));
            return crime;
        } catch (Exception e) {
            showMessage(e.getMessage());
        }
        return null;
    }
    public Crime getCrimeInforFrame(){
        Crime crime = new Crime();


        crime.setIDInmate((txtID.getText()));
        crime.setName(txtName.getText().trim());
        crime.setBirth(txtBirth.getText().trim());
        crime.setGender(boxGenger.getItemAt( boxGenger.getSelectedIndex()).toString());
        crime.setGuilty(txtGuility.getText());
        crime.setRoomNumber(Integer.parseInt(txtRoom.getText()));
        crime.setNumOfPrison(Integer.parseInt(txtYear.getText()));
        return crime;
    }
    public void resetField(){
        txtID.setText("");
        txtName.setText("");
        txtYear.setText("");
        txtBirth.setText("");
        txtGuility.setText("");
        txtRoom.setText("");
        boxGenger.setSelectedIndex(-1);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private DefaultComboBoxModel genderList = new DefaultComboBoxModel();
    private vn.viettuts.qlsv.swing.Combobox boxGenger;
    private vn.viettuts.qlsv.swing.ButtonForm buttonForm1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private vn.viettuts.qlsv.swing.TextFielInForm txtBirth;
    private vn.viettuts.qlsv.swing.TextFielInForm txtGuility;
    private vn.viettuts.qlsv.swing.TextFielInForm txtID;
    private vn.viettuts.qlsv.swing.TextFielInForm txtName;
    private vn.viettuts.qlsv.swing.TextFielInForm txtRoom;
    private vn.viettuts.qlsv.swing.TextFielInForm txtYear;
    // End of variables declaration//GEN-END:variables
}
