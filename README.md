<h1 style="color:#8B4513;border: 2px solid #FF5733;
box-shadow: 2px 2px 5px #888888;text-align: center;width: 100vw;">
CHƯƠNG TRÌNH QUẢN LÝ PHẠM NHÂN TRẠI GIAM</h1>



## Tải dự án về
Mở folder cần chứa dự án, bật terminal sau đó clone:
```
git clone https://gitlab.com/nguyentri908908/maven_need_help.git
```
## Chạy dự án
 - Mở dự án bằng Netbean hoặc IntelliJ.
 - Chọn và chạy file App.java: 

<p align="center">
  <img src="runfile.png"  >
</p>

- Đăng nhập với tài khoản là **admin** và mật khẩu là **admin**:

![Example](intrologin.png)

### Trang chính:

- Bao gồm thông tin trại giam, thống kê số lượng phạm nhân và số lượng lượt thăm nuôi.


- Biểu đồ thống kê lượt thăm nuôi theo quý của từng năm.

![Example](intromain.png)

### Đăng xuất:

<p align="center">
  <img src="logout.png"  width="300">
</p>

### Thêm thông tin phạm nhân:

![Example](introaddcrime.png)

- Nếu Id phạm nhân nhập vào đã tồn tại thì sẽ báo lỗi:

![Example](erroradd.png)
### Quản lý thông tin phạm nhân:

- Bao gồm tìm kiếm, sửa và xóa


- Tìm kiếm bằng cách chọn trường trong combobox sau đó gõ vào ô tìm kiếm và nhấn Enter

![Example](searchcrime.png)

- Sửa bằng cách nhấn vào ô cần sửa rồi ấn nút edit, sau đó sẽ hiện lên 1 frame để sửa

![Example](editcrime.png)

### Thêm danh sách thăm nuôi:

- Nếu Id Phạm nhân không tồn tại sẽ báo lỗi

![Example](addvisit.png)

- Khi click chuột vào tháng hoặc năm sẽ chọn sẽ hiện ra năm, tháng để chọn

![Example](celender.png)

### Quản lý lịch thăm nuôi cũng bao gồm tìm kiếm, sửa, xóa

![Example](editvisit.png)

### Xuất báo cáo dưới dạng file Excel:

- Có thể chọn các tường để sắp xếp, lọc theo ngày tháng năm để xuất báo cáo

![Example](export.png)

- Ví dụ lọc số lượt thăm nuôi vào tháng 11 năm 2024 sẽ thu được như sau:

![Example](excel.png)

